import {
	CommonInhibitors,
	default as CookiecordClient,
	Module,
	command,
	listener,
} from 'cookiecord';
import {
	DELETE_EMOJI,
	clearMessageOwnership,
	ownsBotMessage,
} from '../util/send';
import {
	GuildMember,
	Message,
	MessageActionRow,
	MessageButton,
	MessageEmbed,
	MessageReaction,
	TextChannel,
	ThreadChannel,
	User,
} from 'discord.js';

import ChartJSImage from 'chart.js-image';
import { ThreadAutoArchiveDuration } from 'discord-api-types';
import { suggestionsChannelId } from '../env';

const emojiRegex = /<:\w+?:(\d+?)>|(\p{Emoji_Presentation})/gu;

const defaultPollEmojis = ['✅', '❌'];

export class EtcModule extends Module {
	constructor(client: CookiecordClient) {
		super(client);
	}

	@command({ description: 'See if the bot is alive' })
	async ping(msg: Message) {
		await msg.reply('Pong!').then(m => setTimeout(() => m.edit(`*\`${this.client.ws.ping} ms \`*`), 1000))
	}

	@command({ description: 'Send Some Messages' })
	async info(msg: Message) {
		if (!msg.member?.permissions.has('BAN_MEMBERS')) return;

		function listify(text: string) {
			const indent = '\u200b\u00a0\u00a0\u00a0';
			const bullet = '•';
			return text.replace(/^(\s*)-/gm, `$1${bullet}`).replace(/\t/g, indent);
		}

		var crawen0001 = new MessageEmbed()
			.setColor('#4df590')
			.setTitle('How To Get Help')
			.setDescription(
				listify(`
	- Post your question to <#744254782620827669> in this category.
	    - Dont forget to check <#794594012484862013> before asking questions.
		- It's always ok to just ask your question; you don't need permission.
	- Our bot will make a thread dedicated to answering your channel.
	- Someone will (hopefully!) come along and help you.
	- When your question is resolved, type \`.close\`.
	`))

		var crawen0002 = new MessageEmbed()
			.setColor('#4df590')
			.setTitle('How To Get *Better* Help')
			.setDescription(
				listify(`
	- Explain what you want to happen and why…
	- Show some screenshoot's.
	- Run \`.title <brief description>\` to make your help thread easier to spot.
	- For more tips, check out StackOverflow's guide on **[asking good questions](https://stackoverflow.com/help/how-to-ask)**.`))

		var crawen0003 = new MessageEmbed()
			.setColor('#4df590')
			.setTitle("If You Haven't Gotten Help")
			.setDescription(`Usually someone will try to answer and help solve the issue within a few hours. \ If not, and if you have followed the bullets above, you can ping helpers by running \`.helper\`.`,)

		var crawen0004 = new MessageEmbed()
			.setColor('#fff535')
			.setTitle('How To Give Help')
			.setDescription(listify(`
			- There are a couple ways you can browse help threads:
			- The channel sidebar on the left will list threads you have joined.
			- You can scroll through the channel to see all recent questions.
			- The bot will mark closed questions with ✔️.
			- In the channel, you can click the *⌗*\u2004icon at the top right to view threads by title.`))

		var crawen0005 = new MessageEmbed()
			.setColor('#fff535')
			.setTitle('How To Give *Better* Help')
			.setDescription(
				listify(`
		- Get yourself the <@&951179513734234192> role with reaction role.
		- (If you don't like the pings, you can disable role mentions for the server.)
		- As a <@&951179513734234192>, you can:
		- Run \`.title <brief description>\` to set/update the thread title.
			- This will assist other helpers in finding the thread.
			- Also, it means your help is more accessible to others in the future.
		- If a thread appears to be resolved, run \`.close\` to close it.
			- *Only do this if the asker has indicated that their question has been resolved.*
		`))

		var crawen0006 = new MessageEmbed()
			.setTitle('React with Tick emoji for take Helper role:')
			.setDescription(listify(`
			- You will appear at the top of the role order.
			- You will get special role emoji for you.
			- You will be super cool!
			- You will experience by being exposed to a lot of different situations.
			- You will be more likely to be selected for moderator recruitment.`))
			.setColor('#ff2d2d')
			.setThumbnail('https://emoji.gg/assets/emoji/BugHunter.png')
			.setImage('https://cdn.discordapp.com/attachments/847921246729404416/953736658170286210/cachedImage.png')
			.setFooter({ text: 'Important of all you will win our love and you will have helped so many people. <3', iconURL: 'https://emoji.gg/assets/emoji/7240-partner-white.png' })

		await msg.channel.send({ embeds: [crawen0001, crawen0002, crawen0003, crawen0004, crawen0005, crawen0006] })
	}

	@command({ description: 'Send Some Messages' })
	async offtopicinfo(msg: Message) {
		if (!msg.member?.permissions.has('BAN_MEMBERS')) return;

		let infolink = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setLabel('Document')
					.setStyle('LINK')
					.setURL('https://docs.google.com/document/d/11YaSauY4zxLWob2qC9TEEcTTZPUFNuKTlZssTaG8HQA/edit?usp=sharing')
			);

		function listify(text: string) {
			const indent = '\u200b\u00a0\u00a0\u00a0';
			const bullet = '•';
			return text.replace(/^(\s*)-/gm, `$1${bullet}`).replace(/\t/g, indent);
		}

		let crawen = new MessageEmbed()
			.setColor('#2f3136')
			.setTitle('About OffTopic-General:')
			.setThumbnail('https://cdn.discordapp.com/avatars/879288025903923251/bf493555e9f402a7f97fb2f7e6e1cd19.png?size=2048')
			.setDescription(
				listify(`
**__INFO:__**
Off topic general created for talking topics uncategorized in server so that channel have some rules and have not any slow mode restriction. Like we stated in rules section that channel has harsh penalties with strict rules. You need  offtopic permission role to view the channel and you can get this role after verification.  Actually this is the main chat channel on the server and most people prefer it. Don’t hesitate to use channel or take the offtopic permission role.\n
**__RULES:__**
    __If not implemented causes timeout from server 1 day:__
       - Be respectful to each other and don't use slang.
	   - Don’t talk with flooding
       - Don't alting

	__If not implemented causes timeout from server 3 day:__
	   - Scan with virustotal before sending any download link.
	   - Don’t curse any staff or break their rules.
	   - Don’t advertise anything.
	   - Don’t spam.

	__If not implemented causes timeout from server 1 week:__
	   - Don't use the channel for other than its purpose.
	   - Don’t use channel for server suggestion.
	   - Don’t use channel for support.

	__If not implemented causes kick from server:__
	   - Don’t Persistently use a language other than English.
	   - Don't make fun of any SJW community like LGBT.
	   - Don't make fun of any other community.

	__If not implemented causes ban from server:__
	   - Do not use violence against people psychologically.
	   - Do not threaten to harm people.
	   - Trying to fool the admins.
`))
			.setTimestamp()
			.setFooter({ text: '</> By Ghost Spectre Moderation Team.', iconURL: 'https://cdn.discordapp.com/avatars/950422421427593226/d7d5bb218a64a5987875ec64f13941b0.png?size=2048' })
		await msg.channel.send({ embeds: [crawen], components: [infolink] })
	}

	@listener({ event: 'ready' })
	async buildinfo(msg: Message) {

		function listify(text: string) {
			const indent = '\u200b\u00a0\u00a0\u00a0';
			const bullet = '•';
			return text.replace(/^(\s*)-/gm, `$1${bullet}`).replace(/\t/g, indent);
		}

		let button1703 = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setLabel('x64 / ISO / UPDATE 2')
					.setStyle('LINK')
					.setURL('https://bit.ly/2ZZVoQk')
			);

		let video1703 = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setLabel('Video')
					.setStyle('LINK')
					.setURL('https://youtu.be/ixdsZbkQVJI')
			);	

		let video1909 = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setLabel('Video')
					.setStyle('LINK')
					.setURL('https://youtu.be/guS5ovEdISQ')
			);	

		let video20H2 = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setLabel('Video')
					.setStyle('LINK')
					.setURL('https://youtu.be/B-P1u2LmShc')
			);			

		let video21H2 = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setLabel('Video')
					.setStyle('LINK')
					.setURL('https://youtu.be/nJ418uicpk8')
			);			
						
		let button1909 = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setLabel('x64 / ISO / UPDATE 13')
					.setStyle('LINK')
					.setURL('https://bit.ly/35AeD5R')
			);

		let button1909X86 = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setLabel('x86 / ISO')
					.setStyle('LINK')
					.setURL('https://bit.ly/2PkUEj3')
			);

		let button20H2 = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setLabel('x64 / ISO / UPDATE 3')
					.setStyle('LINK')
					.setURL('https://bit.ly/2UHp69x')
			);

		let button20H2x86 = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setLabel('x86 / ISO')
					.setStyle('LINK')
					.setURL('https://bit.ly/36WUlDz')
			);

		let button21H2 = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setLabel('x64 / ISO / PRO UPDATE 5')
					.setStyle('LINK')
					.setURL('https://ghostcloud.ml/wp1/w1121h222000/')
			);

		let button21H2home = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setLabel('x64 / ISO / HOME')
					.setStyle('LINK')
					.setURL('https://bit.ly/3i8dBUi')
			);
		

		let embed1703 = new MessageEmbed()
			.setColor('#c07d6a')
			.setTitle('[1703] The Best Version For Low End ')
			.setThumbnail('https://cdn.discordapp.com/attachments/963876038511833159/963888513252724757/1703.png')
			.setImage('https://cdn.discordapp.com/attachments/963876038511833159/963888644651909220/unknown.png')
			.setDescription(
				listify(`
- This Build is Based on S Mode, and have such some Compblability issues (such as New Nvidia Drivers, DCH Drivers, Some MS Store Apps, Xbox Game Pass, etc.)\n
- What Works Still on This Build is: Steam, GOG Games, etc. Gotta be Honest, This Build is Lightweight and Less Bloated and Less Telemetry.\n
- I would __DONT__ prefer this build to you if you've a Mid-End PC.\n
- *(This Should Work Fine atleast On 2GB Ram, 1 GHz Speed, and 15GB HDD)*\n
- You can find passwords from video description!`))
			.setFooter({ text: '</> By Ghost Spectre Moderation Team.', iconURL: 'https://cdn.discordapp.com/avatars/879288025903923251/bf493555e9f402a7f97fb2f7e6e1cd19.png?size=2048' })

		let embed1909 = new MessageEmbed()
			.setColor('#deb578')
			.setTitle('[1909] The Best Version For Mid End ')
			.setThumbnail('https://cdn.discordapp.com/attachments/963876038511833159/963888775069581432/1909.png')
			.setImage('https://cdn.discordapp.com/attachments/963876038511833159/963889162086387733/unknown.png')
			.setDescription(
				listify(`
- This Build is Great for Streaming or Multimedia Purposes, and Also a bit Lightweight (If You are Obsessed with Lightweight Builds, Use V-Potato 1703!).\n
- Thats also a Build We Prefer, If You Have a Mid-End PC.\n
- <@821474246718914570> Prefer's this Build.\n
**07/16/2021 - UPDATE13**
- End of Service
- OS build 18363.1679
- Windows Recovery Included (winre)
- Ghost Toolbox rev11
- Note: This Version cannot be update or Windows will forced you to upgrade to Version 20H2/21H1.\n
- You can find passwords from video description!`))
			.setFooter({ text: '</> By Ghost Spectre Moderation Team.', iconURL: 'https://cdn.discordapp.com/avatars/879288025903923251/bf493555e9f402a7f97fb2f7e6e1cd19.png?size=2048' })

		let embed20H2 = new MessageEmbed()
			.setColor('#78b892')
			.setTitle('[2009] / [20H2] The Best Version For High End!')
			.setThumbnail('https://cdn.discordapp.com/attachments/963876038511833159/963889868847579186/20H2.png')
			.setImage('https://cdn.discordapp.com/attachments/963876038511833159/963889986036441128/unknown.png')
			.setDescription(
				listify(`
- This Builds uses More Resources and They are Compatible With Every Program as They are The Latest, For These People Who wants to use Themes, You need to Choose Special Theme in Installation Screen (Selecting Edition Process).\n 
- I would Prefer This Build if You have a High-End PC..\n
- <@696748641053835375>, <@285154906766180362>, <@543571558652968980> Prefer's this build.\n
**05/15/2021 - UPDATE3**
- Update OS build 19042.985
- Fixed error code 0x800f0988 -finger crossed
- Update Defender Engine
- Security intelligence version 1.339.373.0
- Antimalware Client Version: 4.18.1909.6
- Engine Version: 1.1.18100.6
- Antivirus Version: 1.339.373.0
- Antispyware Version: 1.339.373.0\n
- You can find passwords from video description!`))
			.setFooter({ text: '</> By Ghost Spectre Moderation Team.', iconURL: 'https://cdn.discordapp.com/avatars/879288025903923251/bf493555e9f402a7f97fb2f7e6e1cd19.png?size=2048' })

		let embed21H2 = new MessageEmbed()
			.setColor('#8fd0ff')
			.setTitle('[21H2] / [Windows 11] The Latest One!')
			.setThumbnail('https://cdn.discordapp.com/attachments/963876038511833159/963892140390383696/21H2.png')
			.setImage('https://cdn.discordapp.com/attachments/963876038511833159/963892225597640714/unknown.png')
			.setDescription(
				listify(`
- This Builds uses More Resources Even More Than 20H2 and They are Compatible With Every Program as They are The Latest, For These People Who wants to use Themes, You need to Choose Special Theme in Installation Screen (Selecting Edition Process).\n
- We would not Prefer this version due unstablelity and more bloated.\n
- To summarize briefly, It's can even be called themed 20H2 and it's much more bloat.\n
**03/16/2022 - UPDATE 5**
- Update OS Build 22000.556
- Update Startmenu for SE
- Update WinNTSetup for WPE Bootable
- Update Defender Engine
- Security intelligence 1.359.190.0
- Antimalware client 4.18.2104.10
- Engine 1.1.18900.3
- Antivirus 1.359.1900.0
- Antispyware 1.359.1900.0\n
**(Warning: Some UWP Apps Doesnt Work On 21H2!)**.\n
- You can find passwords from video description!`))
			.setFooter({ text: '</> By Ghost Spectre Moderation Team.', iconURL: 'https://cdn.discordapp.com/avatars/879288025903923251/bf493555e9f402a7f97fb2f7e6e1cd19.png?size=2048' })

		await ( this.client.channels.cache.get('963872882507776002') as TextChannel ).bulkDelete(100, true)
		await ( this.client.channels.cache.get('963872882507776002') as TextChannel ).send({ embeds: [embed1703], components: [button1703, video1703] })
		await ( this.client.channels.cache.get('963872882507776002') as TextChannel ).send({ embeds: [embed1909], components: [button1909, button1909X86, video1909] })
		await ( this.client.channels.cache.get('963872882507776002') as TextChannel ).send({ embeds: [embed20H2], components: [button20H2,button20H2x86, video20H2] })
		await ( this.client.channels.cache.get('963872882507776002') as TextChannel ).send({ embeds: [embed21H2], components: [button21H2,button21H2home, video21H2] })
	}


	@command({ description: 'Send Some Messages' })
	async infotoolbox(msg: Message) {
		if (!msg.member?.permissions.has('BAN_MEMBERS')) return;

		let eda1 = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setLabel('Image')
					.setStyle('LINK')
					.setURL('https://i.imgur.com/2sEd0ag.png')
			);

		let eda2 = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setLabel('Image')
					.setStyle('LINK')
					.setURL('https://i.imgur.com/bd1Zcmr.png')
			);

		let eda3 = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setLabel('Image')
					.setStyle('LINK')
					.setURL('https://i.imgur.com/si8nm1d.png')
			);

		let eda4 = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setLabel('Image')
					.setStyle('LINK')
					.setURL('https://i.imgur.com/024a7t3.png')
			);

		let eda5 = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setLabel('Image')
					.setStyle('LINK')
					.setURL('https://i.imgur.com/UF8eNU5.png')
			);

		let eda6 = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setLabel('Image')
					.setStyle('LINK')
					.setURL('https://i.imgur.com/UelIm8Y.png')
			);

		let eda7 = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setLabel('Image')
					.setStyle('LINK')
					.setURL('https://i.imgur.com/zHFWblS.png')
			);

		let eda8 = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setLabel('Image')
					.setStyle('LINK')
					.setURL('https://i.imgur.com/B4EEPtN.png')
			);

		let eda9 = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setLabel('Image')
					.setStyle('LINK')
					.setURL('https://i.imgur.com/9Azcotw.png')
			);

		let eda12 = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setLabel('Image')
					.setStyle('LINK')
					.setURL('https://i.imgur.com/Dmw2g6h.png')
			);

		let eda13 = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setLabel('Image')
					.setStyle('LINK')
					.setURL('https://i.imgur.com/si8nm1d.png')
			);

		let eda14 = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setLabel('Image')
					.setStyle('LINK')
					.setURL('https://i.imgur.com/rcReCsE.png')
			);

		let eda15 = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setLabel('Image')
					.setStyle('LINK')
					.setURL('https://i.imgur.com/mhABxyf.png')
			);

		let eda18 = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setLabel('Image')
					.setStyle('LINK')
					.setURL('https://i.imgur.com/jvyGkUS.png')
			);

		let eda25 = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setLabel('Image')
					.setStyle('LINK')
					.setURL('https://i.imgur.com/46r6xTN.png')
			);

		let eda36 = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setLabel('Image')
					.setStyle('LINK')
					.setURL('https://i.imgur.com/eaX5eTN.png')
			);

		let eda37 = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setLabel('Image')
					.setStyle('LINK')
					.setURL('https://i.imgur.com/MNk6Cgz.png')
			);

		let eda27 = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setLabel('Image')
					.setStyle('LINK')
					.setURL('https://i.imgur.com/1KPOW0m.png')
			);

		let eda10 = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setLabel('Image')
					.setStyle('LINK')
					.setURL('https://i.imgur.com/Km5wOG5.png')
			);

		let eda11 = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setLabel('Image')
					.setStyle('LINK')
					.setURL('https://i.imgur.com/8fNwXks.png')
			);

		let eda19 = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setLabel('Image')
					.setStyle('LINK')
					.setURL('https://i.imgur.com/BC7U5uf.png')
			);

		let eda20 = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setLabel('Image')
					.setStyle('LINK')
					.setURL('https://i.imgur.com/AfPsGoP.png')
			);

		let eda23 = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setLabel('Image')
					.setStyle('LINK')
					.setURL('https://i.imgur.com/Vv4xTEQ.png')
			);

		let eda26 = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setLabel('Image')
					.setStyle('LINK')
					.setURL('https://i.imgur.com/tdBGQfu.png')
			);

		let eda28 = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setLabel('Image')
					.setStyle('LINK')
					.setURL('https://cdn.discordapp.com/attachments/963532626558328862/963702222498168832/unknown.png')
			);

		let eda29 = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setLabel('Image')
					.setStyle('LINK')
					.setURL('https://cdn.discordapp.com/attachments/963532626558328862/963702356359401542/unknown.png')
			);

		let eda30 = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setLabel('Image')
					.setStyle('LINK')
					.setURL('https://i.imgur.com/3vXpeBQ.png')
			);

		let eda42 = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setLabel('Image')
					.setStyle('LINK')
					.setURL('https://i.imgur.com/Byy80Ek.png')
			);

		let eda22 = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setLabel('Image')
					.setStyle('LINK')
					.setURL('https://i.imgur.com/5gt296p.png')
			);

		let eda24 = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setLabel('Image')
					.setStyle('LINK')
					.setURL('https://i.imgur.com/hk7yfT0.png')
			);

		let eda31 = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setLabel('Image')
					.setStyle('LINK')
					.setURL('https://i.imgur.com/9wOTgNb.png')
			);

		let eda32 = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setLabel('Image')
					.setStyle('LINK')
					.setURL('https://i.imgur.com/I69otov.png')
			);

		let eda33 = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setLabel('Image')
					.setStyle('LINK')
					.setURL('https://i.imgur.com/2A6TrCM.png')
			);

		let eda34 = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setLabel('Image')
					.setStyle('LINK')
					.setURL('https://i.imgur.com/Sl3DU56.png')
			);

		let eda35 = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setLabel('Image')
					.setStyle('LINK')
					.setURL('https://i.imgur.com/uCQqw7g.png')
			);

		let eda38 = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setLabel('Image')
					.setStyle('LINK')
					.setURL('https://i.imgur.com/1OxdFbI.png')
			);

		let eda39 = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setLabel('Image')
					.setStyle('LINK')
					.setURL('https://i.imgur.com/sUwfXsE.png')
			);

		let eda40 = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setLabel('Image')
					.setStyle('LINK')
					.setURL('https://i.imgur.com/gU3e2St.png')
			);

		let eda41 = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setLabel('Image')
					.setStyle('LINK')
					.setURL('https://i.imgur.com/gU3e2St.png')
			);

		let eda16 = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setLabel('Image')
					.setStyle('LINK')
					.setURL('https://i.imgur.com/XvTXJqT.png')
			);

		let eda17 = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setLabel('Image')
					.setStyle('LINK')
					.setURL('https://cdn.discordapp.com/attachments/963532626558328862/963706189064065034/unknown.png')
			);

		let eda99 = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setLabel('Image')
					.setStyle('LINK')
					.setURL('https://cdn.discordapp.com/attachments/963532626558328862/963706303786655744/unknown.png')
			);

		function listify(text: string) {
			const indent = '\u200b\u00a0\u00a0\u00a0';
			const bullet = '•';
			return text.replace(/^(\s*)-/gm, `$1${bullet}`).replace(/\t/g, indent);
		}

		let onethtweak = new MessageEmbed()
			.setColor('#df5b61')
			.setTitle('[1] | Action Center & Notification | Cortana | Printer')
			.setThumbnail('https://i.imgur.com/bqb3MqA.png')
			.setImage('https://i.imgur.com/2sEd0ag.png')
			.setDescription(
				listify(`
- __Action Center& Nofitication:__ 
Enables Action Center (in some Builds that have em disabled like V-Potato or V-P8.1\n
- __Cortana:__ 
Enables Cortana back.\n
- __Printer:__ 
Enables the Printer Spooler since a Gaming PC Doesnt like them Enabled.`))
			.setTimestamp()
			.setFooter({ text: '</> By Ghost Spectre Moderation Team.', iconURL: 'https://cdn.discordapp.com/avatars/950422421427593226/d7d5bb218a64a5987875ec64f13941b0.png?size=2048' })

		let secondthtweak = new MessageEmbed()
			.setColor('#D08770')
			.setTitle('[2] | Clear Event Viewer Logs')
			.setThumbnail('https://i.imgur.com/U2WlaSB.png')
			.setImage('https://i.imgur.com/bd1Zcmr.png')
			.setDescription(
				listify(`
- __Clear Event Viewer Logs:__ 
Clear's Event Viewer Logs And Can Disable Some Event Viewers Logs.`))
			.setTimestamp()
			.setFooter({ text: '</> By Ghost Spectre Moderation Team.', iconURL: 'https://cdn.discordapp.com/avatars/950422421427593226/d7d5bb218a64a5987875ec64f13941b0.png?size=2048' })

		let thirthtweak = new MessageEmbed()
			.setColor('#ddde78')
			.setTitle('[3] | Clear Cache Updates | Delivery Optimization')
			.setThumbnail('https://i.imgur.com/Yz36x5Q.png')
			.setImage('https://i.imgur.com/si8nm1d.png')
			.setDescription(
				listify(`
- Clears the Cache claimed from Updates&Delivery Optimization.`))
			.setTimestamp()
			.setFooter({ text: '</> By Ghost Spectre Moderation Team.', iconURL: 'https://cdn.discordapp.com/avatars/950422421427593226/d7d5bb218a64a5987875ec64f13941b0.png?size=2048' })

		let fourthtweak = new MessageEmbed()
			.setColor('#78b892')
			.setTitle('[4] | Ghost Online Activator')
			.setThumbnail('https://i.imgur.com/4Mklxqg.png')
			.setImage('https://i.imgur.com/024a7t3.png')
			.setDescription(
				listify(`
- For enabling Windows, uses KMS.`))
			.setTimestamp()
			.setFooter({ text: '</> By Ghost Spectre Moderation Team.', iconURL: 'https://cdn.discordapp.com/avatars/950422421427593226/d7d5bb218a64a5987875ec64f13941b0.png?size=2048' })

		let fivethtweak = new MessageEmbed()
			.setColor('#8fd0ff')
			.setTitle('[5] | Hibernation | Fastboot | Sleepmode | Sysmain')
			.setThumbnail('https://i.imgur.com/CEiIurd.png')
			.setImage('https://i.imgur.com/UF8eNU5.png')
			.setDescription(
				listify(`
- __Hibernation:__
Disables Hibernation since You just dont need it.
- __Fastboot:__ 
Fastboot Tweaks. if You have any problem with Your Mouse or Keyboard, You can try it.
- __Sleepmode:__ 
Disables Sleepmode.
- __Sysmain:__
Disables Sysmain, the Compressed Memory.`))
			.setTimestamp()
			.setFooter({ text: '</> By Ghost Spectre Moderation Team.', iconURL: 'https://cdn.discordapp.com/avatars/950422421427593226/d7d5bb218a64a5987875ec64f13941b0.png?size=2048' })

		let sixthtweak = new MessageEmbed()
			.setColor('#9d7cd8')
			.setTitle('[6] | Pagefile (virtual memory)')
			.setThumbnail('https://i.imgur.com/X65cWvb.png')
			.setImage('https://i.imgur.com/UelIm8Y.png')
			.setDescription(
				listify(`
- Pagefile (Swap, as in Linux): Disables Pagefile, the Virtual Memory (Not prefered on Machines with Low Ram!`))
			.setTimestamp()
			.setFooter({ text: '</> By Ghost Spectre Moderation Team.', iconURL: 'https://cdn.discordapp.com/avatars/950422421427593226/d7d5bb218a64a5987875ec64f13941b0.png?size=2048' })

		let seventhtweak = new MessageEmbed()
			.setColor('#df5b61')
			.setTitle('[7] | Right click Take Ownership Menu')
			.setThumbnail('https://i.imgur.com/uewcbHq.png')
			.setImage('https://i.imgur.com/zHFWblS.png')
			.setDescription(
				listify(`
- Right click Take Ownership Menu: Enables the Take Ownership Section when you Right Click into Files like in W7.`))
			.setTimestamp()
			.setFooter({ text: '</> By Ghost Spectre Moderation Team.', iconURL: 'https://cdn.discordapp.com/avatars/950422421427593226/d7d5bb218a64a5987875ec64f13941b0.png?size=2048' })

		let eightthtweak = new MessageEmbed()
			.setColor('#D08770')
			.setTitle('[8] | Stops Windows Updates until 2077')
			.setThumbnail('https://i.imgur.com/o50Jsfa.png')
			.setImage('https://i.imgur.com/B4EEPtN.png')
			.setDescription(
				listify(`
- __Stops Windows Updates until 2050:__ 
As in Title, it will be Stopped until 2050.
- __Apps Updates:__
You can Stop Auto Updating from MS Store Here.`))
			.setTimestamp()
			.setFooter({ text: '</> By Ghost Spectre Moderation Team.', iconURL: 'https://cdn.discordapp.com/avatars/950422421427593226/d7d5bb218a64a5987875ec64f13941b0.png?size=2048' })

		let ninethtweak = new MessageEmbed()
			.setColor('#ddde78')
			.setTitle('[9] | Compact | LZX compression')
			.setThumbnail('https://i.imgur.com/5j4CUk8.png')
			.setImage('https://i.imgur.com/9Azcotw.png')
			.setDescription(
				listify(`
- __Compact:__
Disables appX packages. (MS Store Apps)
- __LZX Compression:__
Compresses the File with LZX (a Compression System, https://winaero.com/ntfs-lzx-compress-algorithm-windows-10/.`))
			.setTimestamp()
			.setFooter({ text: '</> By Ghost Spectre Moderation Team.', iconURL: 'https://cdn.discordapp.com/avatars/950422421427593226/d7d5bb218a64a5987875ec64f13941b0.png?size=2048' })

		let twelvethtweak = new MessageEmbed()
			.setColor('#78b892')
			.setTitle('[12] | Microsoft Edge (browser)')
			.setThumbnail('https://i.imgur.com/UuN2Uwx.png')
			.setImage('https://i.imgur.com/Dmw2g6h.png')
			.setDescription(
				listify(`
- Microsoft Edge Chromium: Installs Edge Back.`))
			.setTimestamp()
			.setFooter({ text: '</> By Ghost Spectre Moderation Team.', iconURL: 'https://cdn.discordapp.com/avatars/950422421427593226/d7d5bb218a64a5987875ec64f13941b0.png?size=2048' })

		let thirteenthtweak = new MessageEmbed()
			.setColor('#8fd0ff')
			.setTitle('[13] | Firefox Mozilla (browser)')
			.setThumbnail('https://i.imgur.com/qUNXDKR.png')
			.setImage('https://i.imgur.com/LDUIE70.png')
			.setDescription(
				listify(`
-  Firefox Mozilla: Installs Firefox.`))
			.setTimestamp()
			.setFooter({ text: '</> By Ghost Spectre Moderation Team.', iconURL: 'https://cdn.discordapp.com/avatars/950422421427593226/d7d5bb218a64a5987875ec64f13941b0.png?size=2048' })

		let fourteenthtweak = new MessageEmbed()
			.setColor('#9d7cd8')
			.setTitle('[14] | Google Chrome (browser)')
			.setThumbnail('https://i.imgur.com/D0yjRMe.png')
			.setImage('https://i.imgur.com/rcReCsE.png')
			.setDescription(
				listify(`
- Google Chrome: Installs Chrome.`))
			.setTimestamp()
			.setFooter({ text: '</> By Ghost Spectre Moderation Team.', iconURL: 'https://cdn.discordapp.com/avatars/950422421427593226/d7d5bb218a64a5987875ec64f13941b0.png?size=2048' })

		let fiveteenthtweak = new MessageEmbed()
			.setColor('#df5b61')
			.setTitle('[15] | Daum Potplayer (media player)')
			.setThumbnail('https://i.imgur.com/r5S6mQY.png')
			.setImage('https://i.imgur.com/mhABxyf.png')
			.setDescription(
				listify(`
- Daum Potplayer: Installs Daum (GS doesnt have a media player, duh?)`))
			.setTimestamp()
			.setFooter({ text: '</> By Ghost Spectre Moderation Team.', iconURL: 'https://cdn.discordapp.com/avatars/950422421427593226/d7d5bb218a64a5987875ec64f13941b0.png?size=2048' })

		let eightteenthtweak = new MessageEmbed()
			.setColor('#D08770')
			.setTitle('[18] | Brave (browser)')
			.setThumbnail('https://i.imgur.com/WUGQrjV.png')
			.setImage('https://i.imgur.com/jvyGkUS.png')
			.setDescription(
				listify(`
- Installs Brave.`))
			.setTimestamp()
			.setFooter({ text: '</> By Ghost Spectre Moderation Team.', iconURL: 'https://cdn.discordapp.com/avatars/950422421427593226/d7d5bb218a64a5987875ec64f13941b0.png?size=2048' })

		let e25 = new MessageEmbed()
			.setColor('#ddde78')
			.setTitle('[25] | DriverEasy (Portable)')
			.setThumbnail('https://i.imgur.com/vPMFDo0.png')
			.setImage('https://i.imgur.com/46r6xTN.png')
			.setDescription(
				listify(`
-  Doesnt Install DriverEasy, just Downloads it and Runs it. (running these may reduce your performance, also i prefer snappy or grab the drivers from your manufacturers!)
`))
			.setTimestamp()
			.setFooter({ text: '</> By Ghost Spectre Moderation Team.', iconURL: 'https://cdn.discordapp.com/avatars/950422421427593226/d7d5bb218a64a5987875ec64f13941b0.png?size=2048' })

		let e36 = new MessageEmbed()
			.setColor('#78b892')
			.setTitle('[36] | IObit Driver Booster')
			.setThumbnail('https://i.imgur.com/eY99ZRg.png')
			.setImage('https://i.imgur.com/eaX5eTN.png')
			.setDescription(
				listify(`
- Installs IObits Driver Booster, same things from DriverEz Goes for here too.				
`))
			.setTimestamp()
			.setFooter({ text: '</> By Ghost Spectre Moderation Team.', iconURL: 'https://cdn.discordapp.com/avatars/950422421427593226/d7d5bb218a64a5987875ec64f13941b0.png?size=2048' })

		let e37 = new MessageEmbed()
			.setColor('#8fd0ff')
			.setTitle('[37] | 7-Zip 21.07 - 2021-12-26')
			.setThumbnail('https://i.imgur.com/dC6EHOc.png')
			.setImage('https://i.imgur.com/MNk6Cgz.png')
			.setDescription(
				listify(`
- Installs 7zip as You would need a RAR app for Windows.				
`))
			.setTimestamp()
			.setFooter({ text: '</> By Ghost Spectre Moderation Team.', iconURL: 'https://cdn.discordapp.com/avatars/950422421427593226/d7d5bb218a64a5987875ec64f13941b0.png?size=2048' })

		let e27 = new MessageEmbed()
			.setColor('#9d7cd8')
			.setTitle('[27] | Users Request')
			.setThumbnail('https://i.imgur.com/CeXWOBF.png')
			.setImage('https://i.imgur.com/1KPOW0m.png')
			.setDescription(
				listify(`
- Removed from use for 1.5 years due to copyrights.			
`))
			.setTimestamp()
			.setFooter({ text: '</> By Ghost Spectre Moderation Team.', iconURL: 'https://cdn.discordapp.com/avatars/950422421427593226/d7d5bb218a64a5987875ec64f13941b0.png?size=2048' })

		let e10 = new MessageEmbed()
			.setColor('#df5b61')
			.setTitle('[10] | Microsoft Store & Xbox Console Companion / UWP')
			.setThumbnail('https://i.imgur.com/qjx3DNh.png')
			.setImage('https://i.imgur.com/Km5wOG5.png')
			.setDescription(
				listify(`
- Installs MS Store and Xbox Console Compenion back.				
`))
			.setTimestamp()
			.setFooter({ text: '</> By Ghost Spectre Moderation Team.', iconURL: 'https://cdn.discordapp.com/avatars/950422421427593226/d7d5bb218a64a5987875ec64f13941b0.png?size=2048' })

		let e11 = new MessageEmbed()
			.setColor('#D08770')
			.setTitle('[11] | Microsoft Xbox Game Bar')
			.setThumbnail('https://i.imgur.com/JeJRxnp.png')
			.setImage('https://i.imgur.com/8fNwXks.png')
			.setDescription(
				listify(`
- Installs Xbox Game Bar Back.				
`))
			.setTimestamp()
			.setFooter({ text: '</> By Ghost Spectre Moderation Team.', iconURL: 'https://cdn.discordapp.com/avatars/950422421427593226/d7d5bb218a64a5987875ec64f13941b0.png?size=2048' })

		let e19 = new MessageEmbed()
			.setColor('#ddde78')
			.setTitle('[19] | Microsoft Connect (miracast)')
			.setThumbnail('https://i.imgur.com/vjTaZIk.png')
			.setImage('https://i.imgur.com/BC7U5uf.png')
			.setDescription(
				listify(`
- MS Connect aka miracast: Installs MS Connect Back. 				
`))
			.setTimestamp()
			.setFooter({ text: '</> By Ghost Spectre Moderation Team.', iconURL: 'https://cdn.discordapp.com/avatars/950422421427593226/d7d5bb218a64a5987875ec64f13941b0.png?size=2048' })

		let e20 = new MessageEmbed()
			.setColor('#78b892')
			.setTitle('[20] | Microsoft Clipboard & Touch Keyboard')
			.setThumbnail('https://i.imgur.com/IgifHjy.png')
			.setImage('https://i.imgur.com/AfPsGoP.png')
			.setDescription(
				listify(`
- Enables MS Clipboard & Enables Touch Keyboard				
`))
			.setTimestamp()
			.setFooter({ text: '</> By Ghost Spectre Moderation Team.', iconURL: 'https://cdn.discordapp.com/avatars/950422421427593226/d7d5bb218a64a5987875ec64f13941b0.png?size=2048' })

		let e23 = new MessageEmbed()
			.setColor('#8fd0ff')
			.setTitle('[23] | Microsoft Xbox Game Pass for PC')
			.setThumbnail('https://i.imgur.com/6TcCfDw.png')
			.setImage('https://i.imgur.com/Vv4xTEQ.png')
			.setDescription(
				listify(`
- Installs Xbox Pass back.				
`))
			.setTimestamp()
			.setFooter({ text: '</> By Ghost Spectre Moderation Team.', iconURL: 'https://cdn.discordapp.com/avatars/950422421427593226/d7d5bb218a64a5987875ec64f13941b0.png?size=2048' })

		let e26 = new MessageEmbed()
			.setColor('#9d7cd8')
			.setTitle('[26] | Microsoft OneDrive')
			.setThumbnail('https://i.imgur.com/wpGsvlI.png')
			.setImage('https://i.imgur.com/tdBGQfu.png')
			.setDescription(
				listify(`
- Enables OneDrive.				
`))
			.setTimestamp()
			.setFooter({ text: '</> By Ghost Spectre Moderation Team.', iconURL: 'https://cdn.discordapp.com/avatars/950422421427593226/d7d5bb218a64a5987875ec64f13941b0.png?size=2048' })

		let e28 = new MessageEmbed()
			.setColor('#df5b61')
			.setTitle('[28] | Microsoft Zune Music (Groove Music)')
			.setThumbnail('https://i.imgur.com/tbcadNO.png')
			.setImage('https://cdn.discordapp.com/attachments/963532626558328862/963702222498168832/unknown.png')
			.setDescription(
				listify(`
- Enables Groove Music back.				
`))
			.setTimestamp()
			.setFooter({ text: '</> By Ghost Spectre Moderation Team.', iconURL: 'https://cdn.discordapp.com/avatars/950422421427593226/d7d5bb218a64a5987875ec64f13941b0.png?size=2048' })

		let sude29 = new MessageEmbed()
			.setColor('#D08770')
			.setTitle('[29] | Microsoft Your Phone')
			.setThumbnail('https://i.imgur.com/DNDNMX6.png')
			.setImage('https://cdn.discordapp.com/attachments/963532626558328862/963702356359401542/unknown.png')
			.setDescription(
				listify(`
- Enables Your Phone.			
`))
			.setTimestamp()
			.setFooter({ text: '</> By Ghost Spectre Moderation Team.', iconURL: 'https://cdn.discordapp.com/avatars/950422421427593226/d7d5bb218a64a5987875ec64f13941b0.png?size=2048' })

		let sude30 = new MessageEmbed()
			.setColor('#ddde78')
			.setTitle('[30] | Microsoft .NET Framework')
			.setThumbnail('https://i.imgur.com/aUgNRvC.png')
			.setImage('https://i.imgur.com/3vXpeBQ.png')
			.setDescription(
				listify(`
-  Installs .NET Frameworks (may be requried on some games or programs)		
`))
			.setTimestamp()
			.setFooter({ text: '</> By Ghost Spectre Moderation Team.', iconURL: 'https://cdn.discordapp.com/avatars/950422421427593226/d7d5bb218a64a5987875ec64f13941b0.png?size=2048' })

		let sude42 = new MessageEmbed()
			.setColor('#78b892')
			.setTitle('[42] | Options For Windows 11')
			.setThumbnail('https://i.imgur.com/UdI5w6t.png')
			.setImage('https://i.imgur.com/Byy80Ek.png')
			.setDescription(
				listify(`
- A lot of options for windows 11.			
`))
			.setTimestamp()
			.setFooter({ text: '</> By Ghost Spectre Moderation Team.', iconURL: 'https://cdn.discordapp.com/avatars/950422421427593226/d7d5bb218a64a5987875ec64f13941b0.png?size=2048' })

		let sude22 = new MessageEmbed()
			.setColor('#8fd0ff')
			.setTitle('[22] | Microsoft Disk Benchmark')
			.setThumbnail('https://i.imgur.com/HB5eO4f.png')
			.setImage('https://i.imgur.com/5gt296p.png')
			.setDescription(
				listify(`
- Benchmarks Your Disk. 			
`))
			.setTimestamp()
			.setFooter({ text: '</> By Ghost Spectre Moderation Team.', iconURL: 'https://cdn.discordapp.com/avatars/950422421427593226/d7d5bb218a64a5987875ec64f13941b0.png?size=2048' })

		let sude24 = new MessageEmbed()
			.setColor('#9d7cd8')
			.setTitle('[24] | Ghost Personalize')
			.setThumbnail('https://i.imgur.com/xIhTnfg.png')
			.setImage('https://i.imgur.com/hk7yfT0.png')
			.setDescription(
				listify(`
- Enable the Theme or the Ribbor as you want.			
`))
			.setTimestamp()
			.setFooter({ text: '</> By Ghost Spectre Moderation Team.', iconURL: 'https://cdn.discordapp.com/avatars/950422421427593226/d7d5bb218a64a5987875ec64f13941b0.png?size=2048' })

		let sude31 = new MessageEmbed()
			.setColor('#df5b61')
			.setTitle('[31] | Windows Recovery (winre)')
			.setThumbnail('https://i.imgur.com/GpSmPB1.png')
			.setImage('https://i.imgur.com/9wOTgNb.png')
			.setDescription(
				listify(`
- Opens the Recovery Screen (For these wondered whats winre: https://docs.microsoft.com/en-us/windows-hardware/manufacture/desktop/windows-recovery-environment--windows-re--technical-reference?view=windows-11)			
`))
			.setTimestamp()
			.setFooter({ text: '</> By Ghost Spectre Moderation Team.', iconURL: 'https://cdn.discordapp.com/avatars/950422421427593226/d7d5bb218a64a5987875ec64f13941b0.png?size=2048' })

		let sude32 = new MessageEmbed()
			.setColor('#D08770')
			.setTitle('[32] | Change Windows Editions')
			.setThumbnail('https://i.imgur.com/blUA95T.png')
			.setImage('https://i.imgur.com/I69otov.png')
			.setDescription(
				listify(`
- Changes Windows Editions since you may have Education or Enterprise			
`))
			.setTimestamp()
			.setFooter({ text: '</> By Ghost Spectre Moderation Team.', iconURL: 'https://cdn.discordapp.com/avatars/950422421427593226/d7d5bb218a64a5987875ec64f13941b0.png?size=2048' })

		let sude33 = new MessageEmbed()
			.setColor('#ddde78')
			.setTitle('[33] | Change Administrator name / Add New Users')
			.setThumbnail('https://i.imgur.com/Lza1HKM.png')
			.setImage('https://i.imgur.com/2A6TrCM.png')
			.setDescription(
				listify(`
- Changes the Name of Admin Account / Adds New Users			
`))
			.setTimestamp()
			.setFooter({ text: '</> By Ghost Spectre Moderation Team.', iconURL: 'https://cdn.discordapp.com/avatars/950422421427593226/d7d5bb218a64a5987875ec64f13941b0.png?size=2048' })

		let sude34 = new MessageEmbed()
			.setColor('#78b892')
			.setTitle('[34] | CMD Color Schemes')
			.setThumbnail('https://i.imgur.com/1Vjl1Qu.png')
			.setImage('https://i.imgur.com/Sl3DU56.png')
			.setDescription(
				listify(`
- Enables a Color Scheme listed.			
`))
			.setTimestamp()
			.setFooter({ text: '</> By Ghost Spectre Moderation Team.', iconURL: 'https://cdn.discordapp.com/avatars/950422421427593226/d7d5bb218a64a5987875ec64f13941b0.png?size=2048' })

		let sude35 = new MessageEmbed()
			.setColor('#8fd0ff')
			.setTitle('[35] | Standalone Windows Update / Check latest Updates')
			.setThumbnail('https://i.imgur.com/DAEE6ep.png')
			.setImage('https://i.imgur.com/uCQqw7g.png')
			.setDescription(
				listify(`
-  Updates Windows with a Package / Checks Windows Updates (You take the Responbility of getting the Bloat back by Updating Windows!)			
`))
			.setTimestamp()
			.setFooter({ text: '</> By Ghost Spectre Moderation Team.', iconURL: 'https://cdn.discordapp.com/avatars/950422421427593226/d7d5bb218a64a5987875ec64f13941b0.png?size=2048' })

		let sude38 = new MessageEmbed()
			.setColor('#9d7cd8')
			.setTitle('[38] | Sound (((   Mod   )))')
			.setThumbnail('https://i.imgur.com/F2xzaU6.png')
			.setImage('https://i.imgur.com/1OxdFbI.png')
			.setDescription(
				listify(`
- Installs Some Sound Mods.			
`))
			.setTimestamp()
			.setFooter({ text: '</> By Ghost Spectre Moderation Team.', iconURL: 'https://cdn.discordapp.com/avatars/950422421427593226/d7d5bb218a64a5987875ec64f13941b0.png?size=2048' })

		let sude39 = new MessageEmbed()
			.setColor('#df5b61')
			.setTitle('[39] | Tweaking for Gaming | Etc')
			.setThumbnail('https://i.imgur.com/DWsrUPX.png')
			.setImage('https://i.imgur.com/sUwfXsE.png')
			.setDescription(
				listify(`
- Classical Tweaks such as HPET, etc. 			
`))
			.setTimestamp()
			.setFooter({ text: '</> By Ghost Spectre Moderation Team.', iconURL: 'https://cdn.discordapp.com/avatars/950422421427593226/d7d5bb218a64a5987875ec64f13941b0.png?size=2048' })

		let sude40 = new MessageEmbed()
			.setColor('#D08770')
			.setTitle('[40] | Game Client - Steam/GOG/Origin/Epic/Ubisoft/Battle')
			.setThumbnail('https://i.imgur.com/QDNgkAx.png')
			.setImage('https://i.imgur.com/gU3e2St.png')
			.setDescription(
				listify(`
- Installs a Game Client.  			
`))
			.setTimestamp()
			.setFooter({ text: '</> By Ghost Spectre Moderation Team.', iconURL: 'https://cdn.discordapp.com/avatars/950422421427593226/d7d5bb218a64a5987875ec64f13941b0.png?size=2048' })

		let sude41 = new MessageEmbed()
			.setColor('#ddde78')
			.setTitle('[41] | Ghost Youtube Downloader')
			.setThumbnail('https://i.imgur.com/vxAOl3I.png')
			.setImage('https://i.imgur.com/mUpx8Xq.png')
			.setDescription(
				listify(`
- If you would like to download videos from youtube etc. You can use that service.			
`))
			.setTimestamp()
			.setFooter({ text: '</> By Ghost Spectre Moderation Team.', iconURL: 'https://cdn.discordapp.com/avatars/950422421427593226/d7d5bb218a64a5987875ec64f13941b0.png?size=2048' })

		let sude16 = new MessageEmbed()
			.setColor('#78b892')
			.setTitle('[16] | Visual C++ Redistributables AIO 2022-03-17 (system)')
			.setThumbnail('https://i.imgur.com/3sOSGvj.png')
			.setImage('https://i.imgur.com/XvTXJqT.png')
			.setDescription(
				listify(`
- Installs vcredist Libraries, They are also needed. Why over normal vcredist Libraries with custom libraries?		
`))
			.setTimestamp()
			.setFooter({ text: '</> By Ghost Spectre Moderation Team.', iconURL: 'https://cdn.discordapp.com/avatars/950422421427593226/d7d5bb218a64a5987875ec64f13941b0.png?size=2048' })

		let sude17 = new MessageEmbed()
			.setColor('#8fd0ff')
			.setTitle('[17] | DirectX (system)')
			.setThumbnail('https://i.imgur.com/uYX0mz7.png')
			.setImage('https://cdn.discordapp.com/attachments/963532626558328862/963706189064065034/unknown.png')
			.setDescription(
				listify(`
- Installs DirectX Libraries, They are also Most likely needed.			
`))
			.setTimestamp()
			.setFooter({ text: '</> By Ghost Spectre Moderation Team.', iconURL: 'https://cdn.discordapp.com/avatars/950422421427593226/d7d5bb218a64a5987875ec64f13941b0.png?size=2048' })

		let sude99 = new MessageEmbed()
			.setColor('#9d7cd8')
			.setTitle('[99] | Ghost Toolbox Changelogs / Update')
			.setThumbnail('https://i.imgur.com/iJJLQeg.png')
			.setImage('https://cdn.discordapp.com/attachments/963532626558328862/963706303786655744/unknown.png')
			.setDescription(
				listify(`
- You can see changelogs in here if you intrest. 
`))
			.setTimestamp()
			.setFooter({ text: '</> By Ghost Spectre Moderation Team.', iconURL: 'https://cdn.discordapp.com/avatars/950422421427593226/d7d5bb218a64a5987875ec64f13941b0.png?size=2048' })

		let toolboxlinkembed = new MessageEmbed()
			.setColor('#2f3136')
			.setTitle('If your toolbox has a problem, just download this file:')
			.setThumbnail('https://resources.jetbrains.com/storage/products/toolbox/img/meta/toolbox_logo_300x300.png')
			.setImage('https://i.imgur.com/EeBCCoz.png')
			.setDescription(
				listify(`
- Put the folder which is named Ghost Toolbox into Local Disk C.
- Run the toolbox.updater.x64.exe
__Version:__
\`1.8.8.32 [12/12/2020]\`. 
- Don't worry toolbox has autoupdate.
`))
			.setTimestamp()
			.setFooter({ text: '</> By Ghost Spectre Moderation Team.', iconURL: 'https://cdn.discordapp.com/avatars/950422421427593226/d7d5bb218a64a5987875ec64f13941b0.png?size=2048' })

		let downloadtoolbox = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setLabel('Downlaod Toolbox!')
					.setStyle('LINK')
					.setURL('https://cdn.discordapp.com/attachments/962617737820110868/964079481880981514/Ghost_Toolbox.tar.xz')
			);

		await msg.channel.send({ embeds: [onethtweak], components: [eda1] })
		await msg.channel.send({ embeds: [secondthtweak], components: [eda2] })
		await msg.channel.send({ embeds: [thirthtweak], components: [eda3] })
		await msg.channel.send({ embeds: [fourthtweak], components: [eda4] })
		await msg.channel.send({ embeds: [fivethtweak], components: [eda5] })
		await msg.channel.send({ embeds: [sixthtweak], components: [eda6] })
		await msg.channel.send({ embeds: [seventhtweak], components: [eda7] })
		await msg.channel.send({ embeds: [eightthtweak], components: [eda8] })
		await msg.channel.send({ embeds: [ninethtweak], components: [eda9] })
		await msg.channel.send({ embeds: [twelvethtweak], components: [eda12] })
		await msg.channel.send({ embeds: [thirteenthtweak], components: [eda13] })
		await msg.channel.send({ embeds: [fourteenthtweak], components: [eda14] })
		await msg.channel.send({ embeds: [fiveteenthtweak], components: [eda15] })
		await msg.channel.send({ embeds: [eightteenthtweak], components: [eda18] })
		await msg.channel.send({ embeds: [e25], components: [eda25] })
		await msg.channel.send({ embeds: [e36], components: [eda36] })
		await msg.channel.send({ embeds: [e37], components: [eda37] })
		await msg.channel.send({ embeds: [e27], components: [eda27] })
		await msg.channel.send({ embeds: [e10], components: [eda10] })
		await msg.channel.send({ embeds: [e11], components: [eda11] })
		await msg.channel.send({ embeds: [e19], components: [eda19] })
		await msg.channel.send({ embeds: [e20], components: [eda20] })
		await msg.channel.send({ embeds: [e23], components: [eda23] })
		await msg.channel.send({ embeds: [e26], components: [eda26] })
		await msg.channel.send({ embeds: [e28], components: [eda28] })
		await msg.channel.send({ embeds: [sude29], components: [eda29] })
		await msg.channel.send({ embeds: [sude30], components: [eda30] })
		await msg.channel.send({ embeds: [sude42], components: [eda42] })
		await msg.channel.send({ embeds: [sude22], components: [eda22] })
		await msg.channel.send({ embeds: [sude24], components: [eda24] })
		await msg.channel.send({ embeds: [sude31], components: [eda31] })
		await msg.channel.send({ embeds: [sude32], components: [eda32] })
		await msg.channel.send({ embeds: [sude33], components: [eda33] })
		await msg.channel.send({ embeds: [sude34], components: [eda34] })
		await msg.channel.send({ embeds: [sude35], components: [eda35] })
		await msg.channel.send({ embeds: [sude38], components: [eda38] })
		await msg.channel.send({ embeds: [sude39], components: [eda39] })
		await msg.channel.send({ embeds: [sude40], components: [eda40] })
		await msg.channel.send({ embeds: [sude41], components: [eda41] })
		await msg.channel.send({ embeds: [sude16], components: [eda16] })
		await msg.channel.send({ embeds: [sude17], components: [eda17] })
		await msg.channel.send({ embeds: [sude99], components: [eda99] })
		await msg.channel.send({ embeds: [toolboxlinkembed], components: [downloadtoolbox] })
	}

	@command({ description: 'Eval Thing' })
	async eval(msg: Message) {
		if (!msg.member?.permissions.has('BAN_MEMBERS')) return;
		let args = msg.content.split(' ').slice(1);
		function clean(text: any) {
			if (typeof (text) === "string")
				return text.replace(/`/g, "`" + String.fromCharCode(8203)).replace(/@/g, "@" + String.fromCharCode(8203));
			else
				return text;
		}
		try {
			const code = args.join(" ");
			if (code.includes("token")) return msg.reply("Hayır");
			let evaled = eval(code);

			if (typeof evaled !== "string")
				evaled = require("util").inspect(evaled);

			msg.reply({ embeds: [new MessageEmbed().setColor("RANDOM").setDescription(`:inbox_tray: **Input**\`\`\`${code}\`\`\`:outbox_tray: **Output**\`\`\`xl\n${clean(evaled)}\`\`\``)] });
		} catch (err) {
			console.log(err);
			msg.reply({ embeds: [new MessageEmbed().setTitle("ERROORRR!").setColor("RANDOM").setDescription(`\`\`\`xl\n${clean(err)}\`\`\``)] });
		}
	}

	async onPoll(msg: Message) {
		if (msg.author.bot || !msg.content.toLowerCase().startsWith('poll:'))
			return;
		let emojis = [
			...new Set(
				[...msg.content.matchAll(emojiRegex)].map(x => x[1] ?? x[2]),
			),
		];
		if (!emojis.length) emojis = defaultPollEmojis;
		for (const emoji of emojis) await msg.react(emoji);
	}

	@listener({ event: 'messageCreate' })
	async onSuggestion(msg: Message) {
		if (msg.author.bot || msg.channelId !== suggestionsChannelId) return;
		// First 50 characters of the first line of the content (without cutting off a word)
		const title =
			msg.content
				.split('\n')[0]
				.split(/(^.{0,50}\b)/)
				.find(x => x) ?? 'Suggestion';

		const toTitleCase = (phrase: string) => {
			return phrase
				.toLowerCase()
				.split(' ')
				.map(word => word.charAt(0).toUpperCase() + word.slice(1))
				.join(' ');
		};
		await msg.startThread({
			name: `${toTitleCase(title)} ・ ${toTitleCase(msg.author.username)}`,
			autoArchiveDuration: ThreadAutoArchiveDuration.OneWeek,
		});
		for (let emoji of defaultPollEmojis) await msg.react(emoji);
	}

	@listener({ event: 'threadUpdate' })
	async onSuggestionClose(thread: ThreadChannel) {
		if (
			thread.parentId !== suggestionsChannelId ||
			!((await thread.fetch()) as ThreadChannel).archived
		)
			return;
		const channel = thread.parent!;
		let lastMessage = null;
		let suggestion: Message;
		while (!suggestion!) {
			const msgs = await channel.messages.fetch({
				before: lastMessage ?? undefined,
				limit: 5,
			});
			suggestion = msgs.find(msg => msg.thread?.id === thread.id)!;
			lastMessage = msgs.last()!.id as string;
		}
		const pollingResults = defaultPollEmojis.map(emoji => {
			const reactions = suggestion.reactions.resolve(emoji);
			// Subtract the bot's vote
			const count = (reactions?.count ?? 0) - 1;
			return [emoji, count] as const;
		});
		const pollingResultStr = pollingResults
			.map(([emoji, count], i) => `${count}`)
			.join('  ');

		const myArray = pollingResultStr.split("  ");

		console.log(myArray)


		const ChartJSImage = require('chart.js-image');

		const chart_url = ChartJSImage()
			.chart({
				type: 'doughnut',
				options: {
					legend: {
						display: false //This will do the task
					}
				},
				data: {
					labels: ['Yes', 'No'],
					datasets: [{
						label: 'Results',
						hoverOffset: 1,
						borderColor: '#121212',
						backgroundColor: ['#31ff3f', "#ff2d2d"],
						data: myArray
					}]
				},
			}) // vertical bar chart
			.width(3840) // 
			.height(2160) // 
			.backgroundColor('#121212')
			.toURL(); // get the generated URL

		const clickhere = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setLabel('Click Here!')
					.setStyle('LINK')
					.setURL(chart_url)
			);

		let suggestionembed = new MessageEmbed()
			.setTitle(`Results: ${myArray}`)
			.setColor('#2f3136')
			.setImage(chart_url)
			.setTimestamp()
		await suggestion.reply({
			embeds: [suggestionembed], components: [clickhere],
		});
	}



	@listener({ event: 'messageReactionAdd' })
	async onReact(reaction: MessageReaction, member: GuildMember) {
		if (reaction.partial) return;

		if ((await reaction.message.fetch()).author.id !== this.client.user?.id)
			return;
		if (reaction.emoji.name !== DELETE_EMOJI) return;
		if (member.id === this.client.user?.id) return;

		if (ownsBotMessage(reaction.message, member.id)) {
			clearMessageOwnership(reaction.message);
			await reaction.message.delete();
		} else {
			await reaction.users.remove(member.id);
		}
	}

	@command({
		inhibitors: [CommonInhibitors.botAdminsOnly],
	})
	async kill(msg: Message) {
		const confirm = '✅';
		const confirmationMessage = await msg.channel.send('Confirm?');
		confirmationMessage.react(confirm);
		const reactionFilter = (reaction: MessageReaction, user: User) =>
			reaction.emoji.name === confirm && user.id === msg.author.id;
		const proceed = await confirmationMessage
			.awaitReactions({
				filter: reactionFilter,
				max: 1,
				time: 10 * 1000,
				errors: ['time'],
			})
			.then(() => true)
			.catch(() => false);
		await confirmationMessage.delete();
		if (!proceed) return;
		await msg.react('☠️');
		process.stdout.write(`
                            ,--.
                           {    }
                           K,   }
                          /  ~Y\`
                     ,   /   /
                    {_'-K.__/
                      \`/-.__L._
                      /  ' /\`\\_}
                     /  ' /
             ____   /  ' /
      ,-'~~~~    ~~/  ' /_
    ,'             \`\`~~~  ',
   (                        Y
  {                         I
 {      -                    \`,
 |       ',                   )
 |        |   ,..__      __. Y
 |    .,_./  Y ' / ^Y   J   )|
 \           |' /   |   |   ||      Killed by @${msg.author.username}#${msg.author.discriminator}/${msg.author.id}
  \          L_/    . _ (_,.'(
   \,   ,      ^^""' / |      )
     \_  \          /,L]     /
       '-_~-,       \` \`   ./\`
          \`'{_            )
              ^^\..___,.--\`
		`);
		process.exit(1);
	}

}
