import { Connection, createConnection } from 'typeorm';

import { HelpThread } from './entities/HelpThread';
import { RepGive } from './entities/RepGive';
import { RepUser } from './entities/RepUser';
import { Snippet } from './entities/Snippet';

let db: Connection | undefined;
export async function getDB() {
	if (db) return db;

	// Require ssl in production
	const extraOpts =
		process.env.NODE_ENV === 'production'
			? {
					ssl: true,
					extra: {
						ssl: {
							rejectUnauthorized: false,
						},
					},
				}
			: {};

	db = await createConnection({
		type: "mongodb",
		url: process.env.DATABASE_URL,
		useNewUrlParser: true,
		synchronize: true,
		logging: false,
		entities: [RepUser, RepGive, HelpThread, Snippet],
		...extraOpts,
	});
	console.log('[READY] Connected to DB');
	return db;
}
