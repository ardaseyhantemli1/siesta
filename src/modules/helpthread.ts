/* eslint-disable @typescript-eslint/no-non-null-assertion */
/* eslint-disable no-var */
/* eslint-disable prefer-const */
// @ts-nocheck

import {
	Channel,
	GuildMember,
	Message,
	MessageActionRow,
	MessageButton,
	MessageEmbed,
	TextChannel,
	ThreadChannel
} from 'discord.js';
import { Module, command, listener } from 'cookiecord';
import {
	helpCategory,
	howToGetHelpChannel,
	howToGiveHelpChannel,
	trustedRoleId
} from '../env';

import { HelpThread } from '../entities/HelpThread';
import { ThreadAutoArchiveDuration } from 'discord-api-types';
import db from "quick.db";
import { sendWithMessageOwnership } from '../util/send';

const discordModals = require('discord-modals')
const { Modal, TextInputComponent, showModal } = require('discord-modals')

function UpperWord(string: string) {
    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
} 




const closedEmoji = '☑️';

function listify(text: string) {
	const indent = '\u200b\u00a0\u00a0\u00a0';
	const bullet = '•';
	return text.replace(/^(\s*)-/gm, `$1${bullet}`).replace(/\t/g, indent);
}

const helpInfo = (channel: TextChannel) =>
	new MessageEmbed()
		.setColor('#2f3136')
		.setTitle('Write Your Problem Here, Bot Will Create A Issue For You!')
		.setDescription(channel.topic ?? 'There is no topic LOL!')
		.setTimestamp()
		.setFooter({ text: '</> by seyhan'});

const a = "`";

// The rate limit for thread naming is 2 time / 10 mins, tracked per thread
const titleSetCooldown = 5 * 60 * 1000;

function generateCode() {
    // eslint-disable-next-line prefer-const
    let length = 6,
        charset = "abcdefghijklmnopqrstuvwxyz0123456789",
        retVal = "";
    for (let i = 0, n = charset.length; i < length; ++i) {
        retVal += charset.charAt(Math.floor(Math.random() * n));
    }
    return retVal;
}



export class HelpThreadModule extends Module {
	@listener({ event: 'messageCreate' })
	async onNewQuestion(msg: Message) {
		if (!isHelpChannel(msg.channel)) return;
		if (msg.author.bot) return;
		console.log(
			'Received new question from',
			msg.author,
			'in',
			msg.channel,
		);
		
	const toTitleCase = (phrase) => {
 		 return phrase
   	 .toLowerCase()
    	.split(' ')
   	 .map(word => word.charAt(0).toUpperCase() + word.slice(1))
   	 .join(' ');
};		
		this.updateHelpInfo(msg.channel);
		var generatedcode = generateCode()
		await db.set('dbgeneratedcode', `${generatedcode}`)
		var dbgeneratedcode = await db.get('dbgeneratedcode')
		var uppernickname = toTitleCase(`${msg.member?.nickname ?? msg.author.username}`)
		let thread = await msg.startThread({
			name: `${uppernickname}'s Issue! ・ ${dbgeneratedcode}`,
			autoArchiveDuration: ThreadAutoArchiveDuration.OneWeek,
		});
		await db.set('threadowner31', msg.author.id)
		await db.set('start', Date.now())
		await db.set('avatarurl', msg.author.avatarURL() ?? msg.member?.displayAvatarURL())
		await db.set('UpperNickanmeThreadO', uppernickname)

		const settittlebutton = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setCustomId('settingsbutton')
					.setLabel('Title!')
					.setStyle('DANGER')
			);

		this.client.on('interactionCreate', async interaction => {
   			if (!interaction.isButton()) return;
    		if (interaction.customId == "settingsbutton") {
			var StarterMessage = await interaction.channel.fetchStarterMessage();
			if (
			interaction.user.id !== StarterMessage.member?.id &&
			!interaction.member!.roles.cache.has(trustedRoleId)
		)
			return  await interaction.reply({ content: 'Only Asker And Helpers can Set Title!', ephemeral: true});
        		showModal(modal, {
      			client: this.client, // Client to show the Modal through the Discord API.
      			interaction: interaction // Show the modal with interaction data.
    })}
})
	  	discordModals(this.client)
		this.client.on('modalSubmit', async (modal) => {
	const toTitleCase = (phrase) => {
 		 return phrase
   	 .toLowerCase()
    	.split(' ')
   	 .map(word => word.charAt(0).toUpperCase() + word.slice(1))
   	 .join(' ');
};	
 			if(modal.customId === 'settingsmodal'){
				var dbgeneratedcode = await db.get('dbgeneratedcode')
				db.set(`${dbgeneratedcode}`, 'true')
				const titlecontent = modal.getTextInputValue('settitle')
				if (modal.channel.id == 744254782620827669) {
					modal.followUp({ content: `*Dude Helper! You tring to edit #support channel name at the moment pls click to the thread which is above support channel.*`, ephemeral: true })
				} else {
					modal.channel.setName(`${toTitleCase(titlecontent)} ・ ${dbgeneratedcode}`)
				}
				await modal.deferReply({ ephemeral: true })
				var StarterMessage = await modal.channel.fetchStarterMessage();
				if (modal.user.id == StarterMessage.member?.id) {
					modal.followUp({ content: `*You successfuly set title!*`, ephemeral: true }).then(modal.message.delete())
					StarterMessage.author.send(`Yo Dude Again Me! Quick Reminder: You can use \`.close\` command when issue resolved. Also Dont forget to see <#794594012484862013> channel. Have a good day! ^^`)
				} else {
					modal.followUp({ content: `*Yo Helper!, You successfuly set title!*`, ephemeral: true }).then(modal.message.delete())
					StarterMessage.author.send(`Yo Dude Again Me! Quick Reminder: You can use \`.close\` command when issue resolved. Also Dont forget to see <#794594012484862013> channel. Have a good day! ^^`)
				}
  }  
});

		const modal = new Modal() // We create a Modal
				.setCustomId('settingsmodal')
				.setTitle('Set Title')
				.addComponents(	
			new TextInputComponent() // We create a Text Input Component
				.setCustomId('settitle')
				.setLabel('Please Brief Description')
				.setStyle('LONG') //IMPORTANT: Text Input Component Style can be 'SHORT' or 'LONG' yoooooo
				.setMinLength(8)
				.setMaxLength(70)
				.setPlaceholder('Brief Description Here!')
				.setRequired(true) // If it's re
			);

		var StarterMessage = await thread.fetchStarterMessage();	
		let infoembed = new MessageEmbed()
			.setTitle(`${StarterMessage.author.username}, this issue for your question.`)
			.setColor(`#2f3136`)
			.setDescription(`Please set title with using button.\n When it's resolved please type \`.close\`.`)
			.setTimestamp()

		thread.send({embeds: [infoembed], components: [settittlebutton]})
		msg.author.send(`You succesfuly created new issue which is <#${thread.id}>\`(${dbgeneratedcode})\`. I hope your problem will be solved. Set title for unlock thread, Good luck!`)
 		thread.send(`<@&${trustedRoleId}>, Hey im ghost pinging you because of there is new issue!`).then(m => m.delete())
		await db.set('ThreadId', `${thread.id}`)
		await db.set("ownerisiktimoldu", `${msg.author.id}` )
		await HelpThread.create({
			threadId: thread.id,
			ownerId: msg.author.id,
			origMessageId: msg.id,
		}).save();

		console.log(`Created a new help thread for`, msg.author);
	}

	// Used to differentiate automatic archive from bot archive
	manuallyArchivedThreads = new Set<string>();
	@listener({ event: 'threadUpdate' })
	async onThreadReopen(thread: ThreadChannel) {
		if (
			!isHelpThread(thread) ||
			!thread.archived ||
			((await thread.fetch()) as ThreadChannel).archived
		)
			return;
			const threadData = await db.get("ThreadId");
		if (!threadData.origMessageId) return;
		try {
			const origMessage = await thread.parent.messages.fetch(
				threadData.origMessageId,
			);
			origMessage.reactions
				.resolve(closedEmoji)
				?.users.remove(this.client.user!.id);
		} catch {
			// Asker deleted original message
		}
	}

	@command({
		aliases: ['closed', 'resolved', 'resolve', 'done'],
		description: 'Help System: Close an active help thread',
	})
	async close(msg: Message) {
		if (!isHelpThread(msg.channel))
			return await sendWithMessageOwnership(
				msg,
				':warning: This can only be run in a help thread',
			);

		let thread: ThreadChannel = msg.channel;
		const StarterMessage = await msg.channel.fetchStarterMessage();
		const isOwner = StarterMessage.member?.id === msg.author.id;

		if (
			isOwner ||
			msg.member?.roles.cache.has(trustedRoleId) ||
			msg.member?.permissions.has('MANAGE_MESSAGES')
		) {
			console.log(`Closing help thread:`, thread);
			setTimeout(() => {
				msg.delete();
			}, 1000);
			var dbgeneratedcode = await db.get('dbgeneratedcode')
			if (!isOwner) {
				let StarterMessage = await msg.channel.fetchStarterMessage();
				await msg.channel.send(`Ayo! <@${StarterMessage.member?.id}> your issue \`${dbgeneratedcode}\` was closed by \`${msg.author.username}\`.`)
										try {
							await StarterMessage.author.send(`Hey! Your issue \`${dbgeneratedcode}\` closed by  \`${msg.author.username}\.`);
						}
							catch(err) {
						console.log(err)
						}
		} else {
					var dbgeneratedcode = await db.get('dbgeneratedcode')
					await msg.channel.send(`Issue \`${dbgeneratedcode}\` was closed by asker.`)
						try {
							let StarterMessage = await msg.channel.fetchStarterMessage();
							await StarterMessage.author.send(`Hey! Your issue \`${dbgeneratedcode}\` closed by yourself.`);
						}
							catch(err) {
						console.log(err)
						}
				}
			this.manuallyArchivedThreads.add(thread.id);
			setTimeout(() => {
				thread.setLocked(true);
				this.archiveThread(thread);
			}, 2000)
		} else {
			return await sendWithMessageOwnership(
				msg,
				':warning: You have to be the asker to close the issue.',
			);
		}
	}

	private async archiveThread(thread: ThreadChannel) {
		await thread.setArchived(true);
		const threadData = await db.get("ThreadId");
		if (!threadData.origMessageId) return;
		try {
			const origMessage = await thread.parent!.messages.fetch(
				threadData.origMessageId,
			);
			await origMessage.react(closedEmoji);
		} catch {
			// Asker deleted original message
		}
	}


	private helpInfoLocks = new Map<string, Promise<void>>();
	private updateHelpInfo(channel: TextChannel) {
		this.helpInfoLocks.set(
			channel.id,
			(this.helpInfoLocks.get(channel.id) ?? Promise.resolve()).then(
				async () => {
					await Promise.all([
						...(await channel.messages.fetchPinned()).map(x =>
							x.delete(),
						),
						channel
							.send({ embeds: [helpInfo(channel)] })
							.then(x => x.pin()),
					]);
				},
			),
		);
	}

	@listener({ event: 'messageCreate' })
	deletePinMessage(msg: Message) {
		if (isHelpChannel(msg.channel) && msg.type === 'CHANNEL_PINNED_MESSAGE')
			msg.delete();
	}


	@command()
	async htgh(msg: Message) {
		if (!msg.member?.permissions.has('MANAGE_MESSAGES')) return;
		if (
			msg.channel.id !== howToGetHelpChannel &&
			msg.channel.id !== howToGiveHelpChannel
		)
			return;
		(await msg.channel.messages.fetch()).forEach(x => x.delete());
		msg.channel.send("im gay");
	}
}



export function isHelpChannel(
	channel: Omit<Channel, 'partial'>,
): channel is TextChannel {
	return (
		channel instanceof TextChannel &&
		channel.parentId == helpCategory &&
		channel.id !== '953402242784960542' 
	);
}

export function isHelpThread(
	channel: Omit<Channel, 'partial'>,
): channel is ThreadChannel & { parent: TextChannel } {
	return channel instanceof ThreadChannel && isHelpChannel(channel.parent!);
}
