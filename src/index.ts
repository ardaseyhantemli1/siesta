import { MessageEmbed, TextChannel } from 'discord.js';
import { botAdmins, prefixes, token } from './env';

import { AutoroleModule } from './modules/autorole';
import CookiecordClient from 'cookiecord';
import { EtcModule } from './modules/etc';
import { HelpModule } from './modules/help';
import { HelpThreadModule } from './modules/helpthread';
import { Intents } from 'discord.js';
import { ModModule } from './modules/mod';
import { SnippetModule } from './modules/snippet';
import db from 'quick.db';
import { getDB } from './db';
import moment from 'moment';
import { string } from 'mathjs';

const discordModals = require('discord-modals')
// eslint-disable-next-line @typescript-eslint/no-var-requires
const humanize = require("humanize-duration");

const client = new CookiecordClient(
	{
		botAdmins,
		prefix: prefixes,
	},
	{
		partials: ['REACTION', 'MESSAGE', 'USER', 'CHANNEL'],
		allowedMentions: {
			parse: ['users', 'roles'],
		},
		intents: new Intents([
			'GUILDS',
			'GUILD_MESSAGES',
			'GUILD_MEMBERS',
			'GUILD_MESSAGE_REACTIONS',
		]),
	},
).setMaxListeners(Infinity);

for (const mod of [
	AutoroleModule,
	EtcModule,
	HelpThreadModule,
	HelpModule,
	SnippetModule,
	ModModule,
]) {
	client.registerModule(mod);
}

getDB(); // prepare the db for later


client.login(token);
client.on('ready', async() => {
	console.log(`[READY] Logged in as ${client.user?.tag}`);
let readyembed = new MessageEmbed()
		.setColor(`#31ff3f`)
		.setTitle(`[READY, BOT SUCESSFULY STARTED]`)
		.setDescription(`Logged in as ${client.user?.tag}.\n *Ping: \`${Math.floor(Math.random() * 20)} ms\`*`)
		.setTimestamp();
( client.channels.cache.get('962790130597363823') as TextChannel ).bulkDelete(100, true);
setTimeout(() => ( client.channels.cache.get('962790130597363823') as TextChannel ).send({embeds: [readyembed]}), 1000);
await db.set('start', Date.now())
var startTime = await db.fetch('start');
var maxdelay = 360_000
var mindelay = 720_000
function getRandomDelay(min: number, max: number) {
	return Math.random() * (max - min) + min;
}
setInterval(() => ( client.channels.cache.get('962790130597363823') as TextChannel ).setName(`└─${humanize(Date.now() - startTime, { language: "en", round: true, conjunction: " ", serialComma: false }).toString().replace(/ /g, "-")}`),
getRandomDelay(mindelay, maxdelay));
var asliyisiktim = [".help", ".ping", "Code!", "</> by seyhan#4874", "💖 milfs.", "Have a good day!", "Hit to gym!"]
setInterval(function() {
  var asliyisiktim1= asliyisiktim[Math.floor(Math.random() * (asliyisiktim.length))]
  client.user!.setActivity(`${asliyisiktim1}`, {type: "STREAMING", url: 'https://www.youtube.com/watch?v=Q8Cn-gXTbGQ'});
}, getRandomDelay(3000, 6000));
});

process.on('warning', (warning) => {
  console.warn(warning.name);    
  console.warn(warning.message); 
  console.warn(warning.stack);   
  let warningembed = new MessageEmbed()
		.setColor(`#ffbd05`)
		.setTitle(`[WARNING]`)
		.setDescription(`${warning.name}\n${warning.message}\n${warning.stack}\n *Ping: \`${client.ws.ping} ms\`* `)
		.setThumbnail('https://cdn.pixabay.com/photo/2013/04/01/10/55/attention-98643_1280.png')
		.setTimestamp();
( client.channels.cache.get('962790130597363823') as TextChannel ).send({embeds: [warningembed]})
});

process.on('unhandledRejection', e => {
	console.error('Unhandled rejection', e);
	let unhaldedembed = new MessageEmbed()
		.setColor(`#ff4c4c`)
		.setTitle(`[ERROR]`)
		.setDescription(`${e}.\n*Ping: \`${client.ws.ping} ms\`*`)
		.setThumbnail('https://www.seekpng.com/png/full/334-3345964_error-icon-download-attention-symbol.png')
		.setTimestamp();
// @ts-ignore
let result1 = e.toString().includes('Interaction has already been'); let result2 = e.toString().includes('Unknown Message'); let result3 = e.toString().includes('Thread is archived'); let result4 = e.toString().includes('INTERACTION_ALREADY_REPLIED' ?? 'INTERACTION_NOT_REPLIED'); let result5 = e.toString().includes('CHANNEL_NOT_CACHED'); let result6 = e.toString().includes('TypeORM')
console.log(`${result1.toString()}, ${result2.toString()}, ${result3.toString()}, ${result4.toString()}, ${result5.toString()},  ${result6.toString()}`)
if (result1 || result2 || result3 || result4 || result5 || result6) { 
	return
} else {
( client.channels.cache.get('962790130597363823') as TextChannel ).send({embeds: [unhaldedembed]})
}
});